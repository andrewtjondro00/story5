# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
# from django.template import loader
from .models import Schedule
from .forms import NameForm

response = {}

def index(request):
    schedule = Schedule.objects.all()
    context = {'schedule': schedule}
    return render(request, 'index.html', context)

def addschedule(request):
    if request.method == 'POST':
        form = NameForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    return redirect('/')

def removeschedule(request):
    if request.method == 'POST' and 'id' in request.POST:
        schedule = Schedule.objects.get(id = request.POST['id'])
        schedule.delete()
    return redirect('/')
