from django import forms
from .models import Schedule

class NameForm(forms.ModelForm):
    title = forms.CharField(max_length=200)
    place = forms.CharField(max_length=200)
    date = forms.DateField()
    category = forms.CharField(max_length=200)
    day = forms.CharField(max_length=200)
    hour = forms.TimeField()

    class Meta:
        model = Schedule
        fields = ('title', 'place', 'date', 'category', 'day', 'hour')