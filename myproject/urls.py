from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('addschedule/', views.addschedule, name='addschedule'),
    path('removeschedule/', views.removeschedule, name='removeschedule'),
]