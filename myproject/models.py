# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Schedule(models.Model):
    title = models.CharField(max_length=200)
    place = models.CharField(max_length=200)
    date = models.DateField()
    category = models.CharField(max_length=200)
    day = models.CharField(max_length=200)
    hour = models.TimeField()